# Info

Simple Ubuntu images for build.

# Tags

Ubuntu 20.04/focal (openjdk 11):

* *focal*: packaging
* *focal-jni*: java + maven + gcc
* *focal-maven*: java + maven

Ubuntu 18.04/bionic (openjdk 11):

* *bionic*: packaging
* *bionic-jni*: java + maven + gcc
* *bionic-maven*: java
